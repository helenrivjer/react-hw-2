import { useLayoutEffect, useState } from "react"
import './light_theme.css'

export const useTheme = () => {
    const [theme, setTheme] = useState('light_theme')
    
    useLayoutEffect(() => {    
            document.querySelector('main').classList.toggle('light_theme');    
    }, [theme])
    
    return {theme, setTheme}
}