import React from "react";
import arrow_left_blue from '../../../img/arrow-left-blue.svg'
import arrow_right_blue from '../../../img/arrow-right-blue.svg'
import './profile.css'

export const Profile = () => {
    const cut = () =>{
        document.querySelector('main').classList.add('cut')
        document.querySelector('.arrow').classList.add('hide')
        document.querySelector('.arrow2').classList.remove('hide')
    }
    const standard = () => {
        document.querySelector('main').classList.remove('cut')
        document.querySelector('.arrow').classList.remove('hide')
        document.querySelector('.arrow2').classList.add('hide')    
    }
    return (
        <div className="profile">
            <div className="init">AF</div>
            <div className="login">AnimatedFred<br/><a href="mailto:animated@demo.com">animated@demo.com</a></div>
            <div id="on">
                <img className="arrow" onClick={cut} src={arrow_left_blue} alt='arrow'></img><img className="arrow2 hide" onClick={standard} src={arrow_right_blue} alt='arrow'></img>
            </div>
        </div>
    )
}

