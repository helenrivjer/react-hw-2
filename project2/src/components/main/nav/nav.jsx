import React from "react";
import icon from '../../../img/icon.svg'
import icon2 from '../../../img/icon2.svg'
import icon3 from '../../../img/icon3.svg'
import icon4 from '../../../img/icon4.svg'
import icon5 from '../../../img/icon5.svg'
import icon6 from '../../../img/icon6.svg'

function Nav () {
    return (
    <ul className="nav">
        <li className="nav_item"><img className="search" src={icon}/><input type ='text' placeholder="Search..."></input></li>
        <li className="nav_item"><a href="#"><img src={icon2} alt={2}/><div>Dashboard</div></a></li>
        <li className="nav_item"><a href="#"><img src={icon3} alt={3}/><div>Revenue</div></a></li>
        <li className="nav_item"><a href="#"><img src={icon4} alt={4}/><div>Notifications</div></a></li>
        <li className="nav_item"><a href="#"><img src={icon5} alt={5}/><div>Analytics</div></a></li>
        <li className="nav_item"><a href="#"><img src={icon6} alt={6}/><div>Inventory</div></a></li>
    </ul>
    )
 }

 export default Nav