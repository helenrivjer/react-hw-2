import React from "react";
import logout from '../../../img/logout.svg'
import light from '../../../img/light.svg'
import dark from '../../../img/dark.svg'
import arrow_left from '../../../img/arrow-left.svg'
import arrow_right from '../../../img/arrow-right.svg'
import arrow_left_blue from '../../../img/arrow-left-blue.svg'
import arrow_right_blue from '../../../img/arrow-right-blue.svg'
import { useTheme } from '../../../change_theme/theme'


const Footer = () => {
    const {theme, setTheme} = useTheme()
    const toggle = () => { 
        if (theme === 'light_theme') {
            setTheme('dark_theme');
            document.querySelector('.light').classList.remove('hide')
            document.querySelector('.dark').classList.add('hide')
            document.querySelector('.arrow').src = arrow_left
            document.querySelector('.arrow2').src = arrow_right
            
        } else {
            setTheme('light_theme');
            document.querySelector('.light').classList.add('hide')
            document.querySelector('.dark').classList.remove('hide')
            document.querySelector('.arrow').src = arrow_left_blue
            document.querySelector('.arrow2').src = arrow_right_blue
        }
    }
    return (
        <>
        <footer>
        <ul className="nav">
            <li className="nav_item"><a href="#"><img src={logout} alt={logout}/><div>Logout</div></a></li>
            <li className="nav_item bottom"><a href="#"><img className ='light hide' src={light} alt='light'/><img className ='dark' src={dark} alt='moon'/><div>Light mode</div></a>
                 <div className="switch"><input type="checkbox" onClick={toggle}></input></div>
            </li>
        </ul>
        </footer>
        </>
    )
}
export default Footer
