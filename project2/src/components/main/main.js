import React from 'react';
import {Profile} from './profile/profile';
import Nav from './nav/nav';
import Footer from './footer/footer';


const App = () => { 
    return (
        <>
        <Profile></Profile>
        <Nav></Nav>
        <Footer></Footer>
        </>
    )
}
export default App


 